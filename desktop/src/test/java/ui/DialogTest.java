package ui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * Created by ldh123 on 2018/6/5.
 */
public class DialogTest extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        HBox hbox = new HBox();
        Button b1 = new Button("dialog");
        b1.setOnAction(e->{
            Dialog dialog = new Dialog();
//            dialog.initStyle(StageStyle.UNDECORATED);
            dialog.showAndWait();
        });

        Button b2 = new Button("dialog222");
        b2.setOnAction(e->{
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Information Dialog");
            alert.setHeaderText("Look, an Information Dialog");
            alert.initStyle(StageStyle.UNDECORATED);
            alert.setContentText("I have a great message for you!");

            alert.showAndWait();
        });
        hbox.getChildren().addAll(b1, b2);
        Scene scene = new Scene(hbox, 300, 200);
        primaryStage.setScene(scene);
        primaryStage.show();

    }
}
