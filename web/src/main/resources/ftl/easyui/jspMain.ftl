<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="ctx" value="${'${'}pageContext.request.contextPath${'}'}"/>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>代码自动生成工具</title>
	<link rel="stylesheet" type="text/css" href="${'${'}ctx ${'}'}/resource/frame/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="${'${'}ctx${'}'}/resource/frame/easyui/themes/icon.css">
	<link rel="stylesheet" type="text/css" href="${'${'}ctx${'}'}/resource/common/css/admin.css">
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/common/js/jquery-1.8.0.min.js"></script>
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/frame/easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${'${'}ctx${'}'}/resource/common/js/main.js"></script>
</head>
<body class="easyui-layout">
	<div data-options="region:'north',border:false" style="height:60px;background:#B3DFDA;">
		<center style="font-size:26px">代码自动生成工具</center><span id="website"></span>
		<span style="float:right; margin: 4px 5px 5px 5px"><a href="${'${'}ctx${'}'}/admin/auth/logout">退出</a></span>
	</div>
	<div id="leftPane" data-options="region:'west',split:true,title:'菜单导航'" style="width:150px;">
		<ul class="item">
			<#list tableInfo.tables?keys as key>
			<#if !tableInfo.tables[key].middle && tableInfo.tables[key].create>
			<li><a href="${'${'}ctx ${'}'}/${util.firstLower(tableInfo.tables[key].javaName)}/all">${util.comment(tableInfo.tables[key])}管理</a></li>
			</#if>
			</#list>
		</ul>
	</div>
	<div data-options="region:'center',title:'内容栏'" id="content">
		<iframe id="mainFrame" name="mainFrame" style="width:100%;height:99%" frameborder=no></iframe>
	</div>
</body>
</html>
